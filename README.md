### Esta aplicação é uma adaptação de Scott E. Carpenter <scarpen@ncsu.edu> ###
### Copyright (c) 2014 North Carolina State University
 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
  You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 * 
###


### Clonando o repositorio ###
git clone https://DiiManut@bitbucket.org/DiiManut/ns19-vanet-routing.git 


### Diretório ###

cd ns19-vanet-routing/ns-3.19

### configurar waf ###

./waf configure

### build nas dependências e no script vanet-routing-compare ###

./waf --run scratch/vanet-routing-compare

### Por default os resultados estão para protocolo OLSR ###
### Para gerar relatórios ###

./waf --run "scratch/vanet-routing-compare --CSVfileName=Relatorio-OLSR.csv"


### Os relatorios ficam salvos em ###

ns19-vanet-routing/ns-3.19




