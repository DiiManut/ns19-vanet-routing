#! /usr/bin/env python

# Programs that are runnable.
ns3_runnable_programs = ['build/scratch/ns3.19-scratch-simulator-debug', 'build/scratch/ns3.19-vanet-routing-compare-debug', 'build/scratch/subdir/ns3.19-subdir-debug']

# Scripts that are runnable.
ns3_runnable_scripts = []

